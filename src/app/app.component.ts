import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HammerGestureConfig } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'hammer-menu-slide';

  private sliderPercentageTranslate = 0;
  private sensitivity = 5;
  private menuWidth = 40;
  public isSliderDuringAnimation = false;
  public isMenuActive = false;

  public get SliderTranslateX(): string {
    return 'translateX(' + this.sliderPercentageTranslate + '%)';
  }

  ngOnInit(): void {
    this.goToMainView();
  }

  onPan(ev: HammerInput): void {
    const p = 50 * ev.deltaX / window.innerWidth;
    this.sliderPercentageTranslate = this.isMenuActive ? p : p - 50;
    if (this.isMenuActive) {
      this.sliderPercentageTranslate -= this.menuWidth;
    }
    if (this.sliderPercentageTranslate < -50 && !this.isMenuActive) {
      this.sliderPercentageTranslate = -50;
      return;
    }

    if (ev.isFinal) {
      if (ev.velocityX < -1) {
        this.goToMainView();
      } else if (ev.velocityX > 1) {
        this.showMenuView();
      } else {
        if (p <= -(this.sensitivity / 2)) {
          this.goToMainView();
        } else if (p >= (this.sensitivity / 2)) {
          this.showMenuView();
        } else if (this.isMenuActive) {
          this.showMenuView();
        } else {
          this.goToMainView();
        }
      }
    }
  }

  private showMenuView(): void {
    this.isSliderDuringAnimation = true;
    setTimeout(() => this.isSliderDuringAnimation = false, 400);
    this.sliderPercentageTranslate = -this.menuWidth;
    this.isMenuActive = true;
  }

  private goToMainView(): void {
    this.isSliderDuringAnimation = true;
    setTimeout(() => this.isSliderDuringAnimation = false, 400);
    this.sliderPercentageTranslate = -50;
    this.isMenuActive = false;
  }

}
